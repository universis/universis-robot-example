const { BasicDataContext } = require('@themost/client/common');
const { count } = require('@themost/query');
const {TraceUtils} = require('@themost/common');
const { KeycloakClientService } = require('@universis/keycloak');
const fs = require('fs');
const {DataApplication} = require('@themost/data');
const { JsonLogger } = require('@themost/json-logger');
TraceUtils.useLogger(new JsonLogger());

async function main() {
    // create a simple data application which is based on the current working directory
    // config/app.json contains configuration settings like connection settings to OAuth2 server
    const app = new DataApplication(process.cwd());
    // get settings from keychain
    const [authorizeUser] = app.getConfiguration().getSourceAt('settings/keychain');
    const api = app.getConfiguration().getSourceAt('settings/api/remote');
    // create a new client data context
    const context = new BasicDataContext(api);
    // login to the OAuth2 server
    const client = new KeycloakClientService(app);
    const info = await client.authorize(authorizeUser);
    // set access token
    context.setBearerAuthorization(info.access_token);
    // get stats for department students
    const items = await context.model('Students')
        .select((x) => {
            return {
                department: x.department.name,
                studentStatus: x.studentStatus.name,
                total: count(x.id)
            }
        }).groupBy(
            (x) => {
                return {
                    department: x.department.name,
                    studentStatus: x.studentStatus.name
                }
            }
        ).getItems();
    // and save to a file
    if (fs.existsSync('./tmp') === false) {
        fs.mkdirSync('./tmp');
    }
    fs.writeFileSync('./tmp/students.json', JSON.stringify(items, null, 4));
}

(async () => {
    await main()
})().then(() => {
    process.exit();
}).catch((err) => {
    TraceUtils.error(err);
    process.exit(-1);
});
