## @universis/robot example

This is an example of using the [@universis/robot](https://gitlab.com/universis/robot) package and running scheduled tasks

## Installation

Install `@universis/robot` globally or use `npx` to run the task.

```bash
npm install @universis/robot -g
```

Install project dependencies

```bash
npm install
```

## Usage

This example contains only one task that gets stats for students grouped by student status.
To run the task, use the following command:

```bash
robot --port 0 --config ./json.json
```
or use npx
```bash
npx @universis/robot --port 0 --config ./json.json
```

`jobs.json` file contains the configuration for the task

```json
[
  {
    "name": "Test job",
    "description": "Connect with universis demo api and get stats for students grouped by student status",
    "path": "./job-example.js",
    "interval": "every 1 minute"
  }
]
```


The task will run every 1 minute and will log the result to `tmp/students.json` file.
